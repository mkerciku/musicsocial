// <auto-generated />
namespace GigHUB.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addnametoapplicationuser : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addnametoapplicationuser));
        
        string IMigrationMetadata.Id
        {
            get { return "201806061442307_addnametoapplicationuser"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

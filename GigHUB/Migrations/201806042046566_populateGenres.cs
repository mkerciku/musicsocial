namespace GigHUB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class populateGenres : DbMigration
    {
        public override void Up()
        {
            Sql("insert into Genres values(1, 'Jazz')");
            Sql("insert into Genres values(2, 'Blues')");
            Sql("insert into Genres values(3, 'Rock')");
            Sql("insert into Genres values(4, 'Country')");
        }
        
        public override void Down()
        {
            Sql("delete from Genres where id in(1,2,3,4)");
        }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GigHUB.Startup))]
namespace GigHUB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
